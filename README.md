LocalizeTool
============

A simple tool for detecting variously encoded strings in binary files. Alternative to tools like Radialix and OgreGUI.
